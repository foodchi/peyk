<?php

namespace api\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use common\models\User;
use common\models\Factor;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class FactorController extends ActiveController
{
    public $modelClass = 'common\models\Factor';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        unset($behaviors['contentNegotiator']['formats']['application/xml']);
        return $behaviors;
    }

    public function actionIndex()
    {
        $h = getallheaders();
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            echo json_encode(array('error'=>['code'=>401,'message'=>'Unauthorized']),JSON_PRETTY_PRINT);
            exit();
        }

        $page = 1;
        $limit = 20;

        if (isset(\Yii::$app->request->queryParams['page'])) {
            $page = (int)\Yii::$app->request->queryParams['page'];
        }
        if (isset(\Yii::$app->request->queryParams['limit'])) {
            $limit = (int)\Yii::$app->request->queryParams['limit'];
        }

        $offset = $limit*($page-1);

        $factors = Factor::find()
            ->where([
                'customer_id' => $user->id,
                ]);
        $count = (int)$factors->count();
        $factors = $factors
            ->orderBy("create_time DESC")
            ->offset($offset)
            ->limit($limit)
            ->all();

        $data = [];
        foreach ($factors as $factor) {
            $foods ;
            foreach ($factor->carts as $cart) {
                $foods[] = [
                    'id' => $cart->food->id,
                    'name' => $cart->food->name,
                    'count' => $cart->count,
                    'price' => $cart->food->price,
                    'display_price' => $cart->food->display_price,
                    'image' => $cart->food->imageUri
                ];
            }
            $eachfactor = [
                'id' => $factor->id,
                'create_time' => $factor->create_time,
                'status' => $factor->getStatusName(null, 'en'),
                'price' => $factor->price,
                'address' => [
                    'id' => $factor->address->id,
                    'name' => $factor->address->name,
                    'address' => $factor->address->address
                ],
                'restaurant' => [
                    'id' => $factor->restaurant->id,
                    'name' => $factor->restaurant->name,
                    'description' => $factor->restaurant->description,
                    'stars' => $factor->restaurant->cnt_comment > 0 ? (float)number_format($factor->restaurant->sum_stars/$factor->restaurant->cnt_comment, 2):null,
                    'logo' => $factor->restaurant->getimageUriById($factor->restaurant->logo_id),
                    'cover' => $factor->restaurant->getimageUriById($factor->restaurant->cover_id),
                    'category_id' => $factor->restaurant->category_id,
                ],
                'foods' => $foods

            ];
            $data[] = $eachfactor;
        }

        $meta = [
            'totalCount' => $count,
            'pageCount' => ceil($count/$limit),
            'currentPage' => $page,
            'perPage' => $limit
        ];
        return ['data' => $data, '_meta' => $meta];        
    }

	private function _getStatusCodeMessage($status)
    {
		$codes = [
			200 => 'OK',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
		];
		return (isset($codes[$status])) ? $codes[$status] : '';
	}
	
	private function setHeader($status)
    {
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		$content_type="application/json; charset=utf-8";

		header($status_header);
		header('Content-type: ' . $content_type);
		header('X-Powered-By: ' . "Berkeh <berkehgroup.ir>");
	}

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],$actions['create'],$actions['delete'],$actions['update'],$actions['view']);
        return $actions;
    }
}

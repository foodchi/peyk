<?php

namespace api\controllers;

use common\models\User;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use common\models\Address;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class AddressController extends ActiveController
{
    public $modelClass = 'common\models\Address';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        unset($behaviors['contentNegotiator']['formats']['application/xml']);
        return $behaviors;
    }

    public function actionAdd()
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $params = Yii::$app->request->post();
        if (!isset($params['name']) || !isset($params['address'])) {
            $this->setHeader(400);
            return ['data' => ['status' => 'fail', 'message'=>'Parameter missing']];
        }
        $address = new Address();
        $address->name = $params['name'];
        $address->latitude = isset($params['latitude']) ? $params['latitude']:null;
        $address->longitude = isset($params['longitude']) ? $params['longitude']:null;
        $address->address = $params['address'];
        $address->phone = isset($params['phone']) ? $params['phone']:null;

        $address->user_id = $user->id;
        if (!$address->save()) {
            $this->setHeader(400);
            return ['data' => ['status' => 'fail', 'message'=>$address->errors]];
        }
        $this->setHeader(201);
        return ['data' => [
            'status' => 'success',
            'address' => [
                'id' => $address->id,
                'name' => $address->name,
                'latitude' => $address->latitude,
                'longitude' => $address->longitude,
                'address' => $address->address,
                'phone' => $address->phone,
            ]
        ]];
    }

    public function actionIndex()
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $page = 1;
        $limit = 20;
        if (isset(\Yii::$app->request->queryParams['page'])) {
            $page = (int)\Yii::$app->request->queryParams['page'];
        }
        $offset = $limit*($page-1);
        $addresses = Address::find()->where(['user_id' => $user->id]);
        $count = $addresses->count();
        $addresses = $addresses
            ->offset($offset)
            ->limit($limit)
            ->all();
        $data = [];
        foreach ($addresses as $address) {
            $data[] = [
                'id' => $address->id,
                'name' => $address->name,
                'latitude' => $address->latitude,
                'longitude' => $address->longitude,
                'address' => $address->address,
                'phone' => $address->phone
            ];
        }
        $this->setHeader(200);
        $meta = [
            'totalCount' => $count,
            'pageCount' => ceil($count/$limit),
            'currentPage' => $page,
            'perPage' => $limit
        ];
        return ['data' => $data, '_meta' => $meta];
    }

    public function actionUpdate()
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $params = Yii::$app->request->post();
        if (isset($params['id'])) {
            $model = Address::findOne($params['id']);
            if ($model === null) {
                return ['data' => [
                    'status' => 'fail',
                    'message' => 'not found',
                ]];
            }
            $model->name = isset($params['name']) ? $params['name'] : $model->name;
            $model->latitude = isset($params['latitude']) ? $params['latitude'] : $model->latitude;
            $model->longitude = isset($params['longitude']) ? $params['longitude'] : $model->longitude;
            $model->address = isset($params['address']) ? $params['address'] : $model->address;
            $model->phone = isset($params['phone']) ? $params['phone'] : $model->phone;
            if ($model->save()) {
                return ['data' => [
                    'status' => 'success',
                    'address' => [
                        'id' => $model->id,
                        'name' => $model->name,
                        'latitude' => $model->latitude,
                        'longitude' => $model->longitude,
                        'address' => $model->address,
                        'phone' => $model->phone,
                    ]
                ]];
            } else {
                return ['data' => [
                    'status' => 'fail',
                    'message' => 'parameters missing or incorrect',
                ]];
            }
        } else {
            return ['data' => [
                'status' => 'fail',
                'message' => 'parameters missing',
            ]];
        }
    }

    public function actionDelete($id)
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $model = Address::findOne($id);
        if ($model === null) {
            return ['data' => [
                'status' => 'fail',
                'message' => 'not found',
            ]];
        } else {
            $model->delete();
            return ['data' => [
                'status' => 'success',
                'message' => 'address deleted',
            ]];
        }
    }

    private function _getStatusCodeMessage($status)
    {
		$codes = [
			200 => 'OK',
            201 => 'Created',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
		];
		return (isset($codes[$status])) ? $codes[$status] : '';
	}
	
	private function setHeader($status)
    {
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		$content_type="application/json; charset=utf-8";

		header($status_header);
		header('Content-type: ' . $content_type);
		header('X-Powered-By: ' . "Berkeh <berkehgroup.ir>");
	}

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],$actions['create'],$actions['delete'],$actions['update'],$actions['view']);
        return $actions;
    }
}

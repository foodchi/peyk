<?php

namespace api\controllers;

use common\models\Message;
use common\models\User;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class MessageController extends ActiveController
{
    public $modelClass = 'common\models\Message';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        unset($behaviors['contentNegotiator']['formats']['application/xml']);
        return $behaviors;
    }

    public function actionIndex() {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $page = 1;
        $limit = 20;
        if (isset(\Yii::$app->request->queryParams['page'])) {
            $page = (int)\Yii::$app->request->queryParams['page'];
        }
        $offset = $limit*($page-1);
        $messages = Message::find()
            ->where(['to_user_id' => $user->id])
            ->orWhere(['to_user_id' => null])
            ->offset($offset)
            ->limit($limit)
            ->orderBy('id DESC')
            ->all();

        $msgs = [];
        foreach ($messages as $message) {
            $msgs[] = [
                'id' => $message->id,
                'create_time' => $message->create_time,
                'title' => $message->title,
//                'body' => $message->body,
//                '' => $message->restaurant_id,
//                'food_id' => $message->food_id,
                'image' => $message->imageUri,
                'seen' => $message->status == $message::STATUS_PENDING ? false:true,
            ];
        }
        return ['data' => $msgs];
    }

    public function actionView($id)
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $message = Message::find()->where(['id' => $id, 'to_user_id' => $user->id])->one();
        if ($message) {
            $message->status = $message::STATUS_PUBLISH;
            $message->save();
            return ['data' => [
                'id' => $message->id,
                'create_time' => $message->create_time,
                'title' => $message->title,
                'body' => $message->body,
                'image' => $message->imageUri,
            ]];
        }
        $this->setHeader(404);
        return ['data' => [
            'status' => 'fail',
            'message' => 'message not found',
        ]];
    }


	private function _getStatusCodeMessage($status) {
		$codes = [
			200 => 'OK',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
		];
		return (isset($codes[$status])) ? $codes[$status] : '';
	}
	
	private function setHeader($status) {
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		$content_type="application/json; charset=utf-8";

		header($status_header);
		header('Content-type: ' . $content_type);
		header('X-Powered-By: ' . "Berkeh <berkehgroup.ir>");
	}

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],$actions['create'],$actions['delete'],$actions['update'],$actions['view']);
        return $actions;
    }
}

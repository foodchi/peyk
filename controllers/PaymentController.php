<?php

namespace api\controllers;

use common\models\Cart;
use common\models\Factor;
use common\models\Payment;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\web\Response;
use common\models\User;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class PaymentController extends ActiveController
{
    public $modelClass = 'common\models\Payment';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'pay' => ['post'],
            ],
        ];
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        unset($behaviors['contentNegotiator']['formats']['application/xml']);
        return $behaviors;
    }

    public function actionPay()
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $params = Yii::$app->request->post();
        if (!isset($params['payment_id'], $params['method'])) {
            $this->setHeader(400);
            return ['data' => ['status' => 'fail', 'message'=>'invalid parameters']];
        }
        $payment = Payment::find()->where(['res_num' => $params['payment_id']])->one();
        if ($payment === null) {
            $this->setHeader(400);
            return ['data' => ['status' => 'fail', 'message'=>'invalid parameters']];
        }
        if ($params['method'] == 'presence') {
            $factors = Factor::find()->where(['payment_id' => $payment->id])->all();
            foreach ($factors as $factor) {
                Cart::updateAll(['status' => Factor::STATUS_PUBLISH], ['factor_id' => $factor->id]);
            }
            Factor::updateAll(['status' => Factor::STATUS_PUBLISH], ['payment_id' => $payment->id]);
            $payment->status = $payment::STATUS_WAITING;
            $payment->save();
        } elseif ($params['method'] == 'charge') {
            if ($user->balance < $payment->discounted_price) {
                return ['data' => [
                    'status' => 'fail',
                    'message' => 'balance is lower than price',
                ]];
            }
            $user->balance -= $payment->discounted_price;
            $user->save();

            $factors = Factor::find()->where(['payment_id' => $payment->id])->all();
            foreach ($factors as $factor) {
                Cart::updateAll(['status' => Factor::STATUS_PUBLISH], ['factor_id' => $factor->id]);
            }
            Factor::updateAll(['status' => Factor::STATUS_PUBLISH], ['payment_id' => $payment->id]);
            $payment->status = $payment::STATUS_PUBLISH;
            $payment->save();
        }
        $payment->giveInviterReward();
        return ['data' => [
            'status' => 'success',
            'message' => 'factors paid',
        ]];
    }

    public function actionCharge($amount)
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $payment = new Payment();
        $payment->status = $payment::STATUS_WAITING;
        $payment->type = $payment::TYPE_ONLINE_CHARGE;
        $payment->res_num = intval(microtime(true)*100);
        $payment->total_price = $amount;
        $payment->user_id = $user->id;
        if ($payment->save()) {
//            $user->balance += $payment->total_price;
//            $user->save();
        } else {
            var_dump($payment->errors);
            exit();
        }
        
        return ['data' => [
            'redirect_url' => Url::to(['payment/redirect', ['payment_id' => $payment->res_num]]),
            'payment_id' => $payment->res_num,
        ]];
    }

    public function actionRedirect($payment_id)
    {
        // redirect to bank
        $this->redirect(['payment/bank-response', 'payment_id' => $payment_id]);
    }

    public function actionBankResponse($payment_id)
    {
//        $params = Yii::$app->request->post();
        $params['paymentId'] = $payment_id;
        $payment = Payment::find()->where(['res_num' => $params['paymentId'], 'status' => Payment::STATUS_WAITING])->one();
        if ($payment === null) {
            $this->setHeader(400);
            return ['data' => ['status' => 'fail', 'message'=>'incorrect payment id']];
        }
        $payment->status = Payment::STATUS_PUBLISH;
//        if ()
        $payment->save();
        $payment->user->balance += $payment->total_price;
        $payment->user->save();
        // checking bank response
        Yii::$app->response->format = Response::FORMAT_HTML;
        echo '
<html>
    <head></head>
    <body>
        <h2 style="text-align: center">پرداخت با موفقیت انجام شد.</h2>
        <h4 style="text-align: center">می‌توانید مرورگر را ببندید و به فودچی برگردید</h4>
        <script type="text/javascript">
            window.close();
        </script>
    </body>
</html>';
    }

    public function actionVerify($payment_id)
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }

        $payment = Payment::find()->where(['user_id' => $user->id, 'res_num' => $payment_id])->one();
        if ($payment === null) {
            $this->setHeader(400);
            return ['data' => ['status' => 'fail', 'message'=>'incorrect payment id']];
        }
        return ['data' => [
            'status' => $payment->getStatusName(null, 'en'),
        ]];
    }

    public function actionIndex()
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $data = [];

        $payments = Payment::find()
            ->where(['and',
                ['user_id' => $user->id],
                ['<>', 'status', Payment::STATUS_PENDING],
                ['in', 'type', (new Payment)->getBuyPaymentTypes()]
            ])
            ->orderBy('create_time DESC')
            ->all();
        if (count($payments) > 0) {
            foreach ($payments as $payment) {
                $pendingCount = Factor::find()->where(['payment_id' => $payment->id, 'status' => Factor::STATUS_PUBLISH])->count();
                $data[] = [
                    'id' => $payment->res_num,
//                    'payment_id' => $payment->res_num,
                    'create_time' => intval($payment->res_num/100),
//                    'pay_status' => $payment->getStatusName(null, 'en'),
                    'order_status' => $pendingCount > 0 ? 'waiting':'delivered',
                ];
            }
        }
        return ['data' => $data];
    }

    public function actionView($id)
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $data = [];

        $payment = Payment::find()->where(['res_num' => $id])->one();
        if ($payment === null) {
            $this->setHeader(400);
            return ['data' => ['status' => 'fail', 'message'=>'Invalid parameters']];
        }
        $factorList = [];
        $foodList = [];
        $factors = Factor::find()->where(['payment_id' => $payment->id])->all();
        foreach ($factors as $factor) {
            $factorList[] = [
                'restaurant_id' => $factor->restaurant_id,
                'restaurant_name' => $factor->restaurant->name,
                'price' => $factor->price,
                'discounted_price' => $factor->price,
                'delivery_price' => 5000,
            ];
            $carts = Cart::findAll(['factor_id' => $factor->id]);
            if (count($carts) > 0) {
                foreach ($carts as $cart) {
                    $foodList[] = [
                        'food_id' => $cart->food->id,
                        'food_name' => $cart->food->name,
                        'restaurant_id' => $factor->restaurant_id,
                        'restaurant_name' => $factor->restaurant->name,
                        'count' => $cart->count,
                        'display_price' => $cart->food->display_price,
                        'price' => $cart->food->price,
                        'image' => $cart->food->imageUri
                    ];
                }
            }
        }
        return ['data' => ['order_items' => $foodList, 'factors' => $factorList]];
    }

    private function _getStatusCodeMessage($status) {
		$codes = [
			200 => 'OK',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
		];
		return (isset($codes[$status])) ? $codes[$status] : '';
	}

	private function setHeader($status) {
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		$content_type="application/json; charset=utf-8";

		header($status_header);
		header('Content-type: ' . $content_type);
		header('X-Powered-By: ' . "Berkeh <berkehgroup.ir>");
	}

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],$actions['create'],$actions['delete'],$actions['update'],$actions['view']);
        return $actions;
    }
}

<?php

namespace api\controllers;

use yii\rest\ActiveController;
use yii\web\Response;
use common\models\Restaurant;
use common\models\RestaurantSearch;
// use common\models\Content;
// use common\models\ContentCategory;
// use common\models\Mobile;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class RestaurantController extends ActiveController
{
    public $modelClass = 'common\models\Restaurant';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        unset($behaviors['contentNegotiator']['formats']['application/xml']);
        return $behaviors;
    }

    public function actionIndex($category_id = null)
    {
        $page = 1;
        $limit = 20;
        if (isset(\Yii::$app->request->queryParams['page'])) {
            $page = (int)\Yii::$app->request->queryParams['page'];
        }
        if (isset(\Yii::$app->request->queryParams['per-page'])) {
            $limit = (int)\Yii::$app->request->queryParams['per-page'];
        }
        $offset = $limit*($page-1);

        $restaurants = Restaurant::find()
            ->select(['id', 'status', 'name', 'sum_stars', 'cnt_comment', 'has_delivery', 'delivery_cost', 'preparation_time', 'self_discount', 'logo_id', 'category_id'])
            ->andWhere(['in', 'status', [Restaurant::STATUS_OPEN, Restaurant::STATUS_CLOSED]]);
        if ($category_id) {
            $restaurants = $restaurants->andWhere(['category_id' => (int)$category_id]);
        }
        $count = (int)$restaurants->count();
        $restaurants = $restaurants
            ->offset($offset)
            ->orderBy('status')
            ->limit($limit)
            ->all();
        $data = [];
        foreach ($restaurants as $restaurant) {
            $data[] = [
                'id' => $restaurant->id,
                'name' => $restaurant->name,
                'stars' => $restaurant->cnt_comment > 0 ? (float)number_format($restaurant->sum_stars/$restaurant->cnt_comment, 2):null,
                'is_open' => $restaurant->boolStatusName,
                'has_delivery' => (bool)$restaurant->has_delivery,
                'delivery_cost' => $restaurant->delivery_cost,
                'preparation_time' => $restaurant->preparation_time,
                'capacity' => 10,
                'self_discount' => $restaurant->self_discount,
                'logo' => $restaurant->getimageUriById($restaurant->logo_id),
                // 'cover' => $restaurant->getimageUriById($restaurant->cover_id),
                'category_id' => $restaurant->category_id,
            ];
        }
        $meta = [
            'totalCount' => $count,
            'pageCount' => ceil($count/20),
            'currentPage' => $page,
            'perPage' => $limit
        ];
        return ['data' => $data, '_meta' => $meta];
    }

    public function actionNear($latitude, $longitude, $category_id = null)
    {
        $page = 1;
        $limit = 20;
        if (isset(\Yii::$app->request->queryParams['page'])) {
            $page = (int)\Yii::$app->request->queryParams['page'];
        }
        if (isset(\Yii::$app->request->queryParams['per-page'])) {
            $limit = (int)\Yii::$app->request->queryParams['per-page'];
        }
        $offset = $limit*($page-1);
        
        $restaurants = Restaurant::find()
            ->select(['id', 'status', 'name', 'sum_stars', 'cnt_comment', 'has_delivery', 'delivery_cost', 'preparation_time', 'self_discount', 'logo_id', 'category_id',
                'SQRT(POWER(latitude - '.$latitude.', 2) + POWER(longitude - '.$longitude.', 2)) as distance'])
            ->andWhere(['in', 'status', [Restaurant::STATUS_OPEN, Restaurant::STATUS_CLOSED]]);
        if ($category_id) {
            $restaurants = $restaurants->andWhere(['category_id' => (int)$category_id]);
        }
        $count = (int)$restaurants->count();
        $restaurants = $restaurants
            ->offset($offset)
            ->limit($limit)
            ->orderBy('status,distance')
            ->all();
        $data = [];
        foreach ($restaurants as $restaurant) {
            $data[] = [
                'id' => $restaurant->id,
                'name' => $restaurant->name,
                'stars' => $restaurant->cnt_comment > 0 ? (float)number_format($restaurant->sum_stars/$restaurant->cnt_comment, 2):null,
                'is_open' => $restaurant->boolStatusName,
                'has_delivery' => (bool)$restaurant->has_delivery,
                'delivery_cost' => $restaurant->delivery_cost,
                'preparation_time' => $restaurant->preparation_time,
                'capacity' => 10,
                'self_discount' => $restaurant->self_discount,
                'logo' => $restaurant->getimageUriById($restaurant->logo_id),
                // 'cover' => $restaurant->getimageUriById($restaurant->cover_id),
                'category_id' => $restaurant->category_id,
                'distance' => (float)$restaurant->distance
            ];
        }
        $meta = [
            'totalCount' => $count,
            'pageCount' => ceil($count/20),
            'currentPage' => $page,
            'perPage' => $limit
        ];
        return ['data' => $data, '_meta' => $meta];
    }

    public function actionTop($by = 'star', $category_id = null)
    {
        $page = 1;
        $limit = 20;
        if (isset(\Yii::$app->request->queryParams['page'])) {
            $page = (int)\Yii::$app->request->queryParams['page'];
        }
        if (isset(\Yii::$app->request->queryParams['per-page'])) {
            $limit = (int)\Yii::$app->request->queryParams['per-page'];
        }
        $offset = $limit*($page-1);
        
        $restaurants = Restaurant::find()
            ->select(['id', 'status', 'name', 'sum_stars', 'cnt_comment', 'has_delivery', 'delivery_cost', 'preparation_time', 'self_discount', 'logo_id', 'category_id'])
            ->andWhere(['in', 'status', [Restaurant::STATUS_OPEN, Restaurant::STATUS_CLOSED]]);
        if ($category_id) {
            $restaurants = $restaurants->andWhere(['category_id' => (int)$category_id]);
        }
        $count = (int)$restaurants->count();
        $restaurants = $restaurants
            ->offset($offset)
            ->limit($limit)
            ->orderBy('status, `sum_stars`/`cnt_comment`')
            ->all();
        $data = [];
        foreach ($restaurants as $restaurant) {
            $data[] = [
                'id' => $restaurant->id,
                'name' => $restaurant->name,
                'stars' => $restaurant->cnt_comment > 0 ? (float)number_format($restaurant->sum_stars/$restaurant->cnt_comment, 2):null,
                'is_open' => $restaurant->boolStatusName,
                'has_delivery' => (bool)$restaurant->has_delivery,
                'delivery_cost' => $restaurant->delivery_cost,
                'preparation_time' => $restaurant->preparation_time,
                'capacity' => 10,
                'self_discount' => $restaurant->self_discount,
                'logo' => $restaurant->getimageUriById($restaurant->logo_id),
                // 'cover' => $restaurant->getimageUriById($restaurant->cover_id),
                'category_id' => $restaurant->category_id,
            ];
        }
        $meta = [
            'totalCount' => $count,
            'pageCount' => ceil($count/20),
            'currentPage' => $page,
            'perPage' => $limit
        ];
        return ['data' => $data, '_meta' => $meta];
    }

    public function actionView($id)
    {
        $model = Restaurant::findOne($id);
        if ($model !== null) {
            return [
                'id' => $model->id,
                'create_time' => $model->create_time,
                'is_open' => $model->boolStatusName,
                'name' => $model->name,
                'description' => $model->description,
                'open_from1' => $model->open_from1,
                'open_to1' => $model->open_to1,
                'open_from2' => $model->open_from2,
                'open_to2' => $model->open_to2,
                'open_from3' => $model->open_from3,
                'open_to3' => $model->open_to3,
                'stars' => $model->cnt_comment > 0 ? (float)number_format($model->sum_stars/$model->cnt_comment, 2):null,
                'address' => $model->address,
                'has_delivery' => (bool)$model->has_delivery,
                'delivery_cost' => $model->delivery_cost,
                'preparation_time' => $model->preparation_time,
                'capacity' => 10,
                'self_discount' => $model->self_discount,
                'longitude' => $model->longitude,
                'latitude' => $model->latitude,
                'logo' => $model->getimageUriById($model->logo_id),
                'cover' => $model->getimageUriById($model->cover_id),
                // 'user_id' => $model->user_id,
                'category_id' => $model->category_id,
            ];
        } else {
            return ['data' => [
                'status' => 'fail',
                'message' => 'not found',
            ]];
        }
    }

    private function _getStatusCodeMessage($status) {
        $codes = [
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        ];
        return (isset($codes[$status])) ? $codes[$status] : '';
    }
    
    private function setHeader($status) {
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        $content_type="application/json; charset=utf-8";

        header($status_header);
        header('Content-type: ' . $content_type);
        header('X-Powered-By: ' . "Berkeh <berkehgroup.ir>");
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create'],$actions['delete'],$actions['update']);
        return $actions;
    }
}

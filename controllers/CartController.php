<?php

namespace api\controllers;

use common\models\Discount;
use common\models\Factor;
use common\models\Payment;
use Yii;
use yii\filters\VerbFilter;
use yii\rest\ActiveController;
use yii\web\Response;
use common\models\Cart;
use common\models\Address;
use common\models\Restaurant;
use common\models\User;
use common\models\Food;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CartController extends ActiveController
{
    public $modelClass = 'common\models\Cart';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'order' => ['post'],
                'add' => ['post'],
                'invoice' => ['post'],
            ],
        ];
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        unset($behaviors['contentNegotiator']['formats']['application/xml']);
        return $behaviors;
    }

    public function actionOrder()
    {
        $params = Yii::$app->request->post();
        $user = null;
        $h = getallheaders();
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        if (isset($params['food']) && is_array($params['food'])) {
            Cart::deleteAll(['user_id' => $user->id, 'status' => Cart::STATUS_PENDING]);
            $foodStates = [];
            $totalPrice = 0;
            foreach ($params['food'] as $foodId => $count) {
                if (!is_numeric($count)) {
                    $foodStates[$foodId] = [
                        'food_id' => $foodId,
                        'status' => 'fail',
                        'message' => 'invalid count number'
                    ];
                    continue;
                }
                $food = Food::find()->where(['id' => $foodId])->limit(1)->one();
                if ($food === null) {
                    $foodStates[$foodId] = [
                        'food_id' => $foodId,
                        'status' => 'fail',
                        'message' => 'invalid food id'
                    ];
                    continue;
                }
                if ($food->status != Food::STATUS_READY) {
                    $foodStates[$foodId] = [
                        'food_id' => $foodId,
                        'status' => 'fail',
                        'message' => 'food is not ready',
                    ];
                    continue;
                }
                $cart = new Cart();
                $cart->count = $count;
                $cart->food_id = $food->id;
                // $cart->factor_id = $factor->id;
                $cart->user_id = $user->id;
                if ($cart->save()) {
                    $foodStates[$foodId] = [
                        'food_id' => $foodId,
                        'status' => 'success',
                        'message' => 'added to cart',
                        'price' => $food->price * $count,
                    ];
                    $totalPrice += $food->price * $count;
                } else {
                    $foodStates[$foodId] = [
                        'food_id' => $foodId,
                        'status' => 'fail',
                        'message' => 'internal server error',
                    ];
                }
            }
            $this->setHeader(201);
            return ['data' => [
                'foods' => $foodStates,
                'totalPrice' => $totalPrice,
            ]];
        }
        $this->setHeader(400);
        return ['data' => [
            'status' => 'fail',
            'message' => 'parameter missing',
        ]];
    }

    public function actionAdd()
    {
        $params = Yii::$app->request->post();
        $user = null;
        $h = getallheaders();
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        if (isset($params['food_id']) && !empty($params['food_id']) && is_numeric($params['food_id'])
            && isset($params['count']) && !empty($params['count']) && is_numeric($params['count'])) {
            $food = Food::find()->where(['id' => $params['food_id']])->limit(1)->one();
            if ($food === null) {
                $this->setHeader(400);
                return ['data' => [
                    'status' => 'fail',
                    'message' => 'food_id is invalid',
                ]];
            }
            if ($food->status != Food::STATUS_READY) {
                $this->setHeader(400);
                return ['data' => [
                    'status' => 'fail',
                    'message' => 'food is not ready',
                ]];
            }
            $cart = Cart::find()->where([
                'food_id' => $food->id,
                'user_id' => $user->id,
                'status' => Food::STATUS_PENDING
            ])
                ->limit(1)
                ->one();
            if ($cart === null) {
                /*
                $factor = Factor::find()->where([
                    'customer_id' => $user->id,
                    'restaurant_id' => $food->restaurant_id,
                    'status' => Food::STATUS_PENDING
                ])->limit(1)->one();
                if ($factor === null) {
                    $factor = new Factor();
                    $factor->customer_id = $user->id;
                    $factor->restaurant_id = $food->restaurant_id,
                    $factor->price = 0;
                    $factor->save();
                }*/
                $cart = new Cart();
                $cart->count = $params['count'];
                $cart->food_id = $food->id;
                // $cart->factor_id = $factor->id;
                $cart->user_id = $user->id;
            } else {
                $cart->count += $params['count'];
            }
            $cart->save();
            $this->setHeader(201);
            return ['data' => [
                'status' => 'success',
                'message' => 'added to card',
            ]];
        }
        $this->setHeader(400);
        return ['data' => [
            'status' => 'fail',
            'message' => 'parameter missing',
        ]];
    }

    public function actionIndex()
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $carts = Cart::find()
            ->andWhere([
                'user_id' => $user->id,
                'status' => Cart::STATUS_PENDING
            ])
            ->all();
        $data = [];
        foreach ($carts as $cart) {
            $data[] = [
                'count' => $cart->count,
                'food' => [
                    'id' => $cart->food->id,
                    'create_time' => $cart->food->create_time,
                    'is_ready' => $cart->food->boolStatusName,
                    'name' => $cart->food->name,
                    'description' => $cart->food->description,
                    'display_price' => $cart->food->display_price,
                    'price' => $cart->food->price,
                    'stars' => $cart->food->cnt_comment > 0 ? (float)number_format($cart->food->sum_stars/$cart->food->cnt_comment, 2):null,
                    'raw_material' => $cart->food->raw_material,
                    'image' => $cart->food->imageUri
                ]
            ];
        }
        return ['data' => $data];
    }

    /**
     * @internal $discount \Common\models\Discount
     * @return array
     */
    public function actionInvoice()
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }

        $params = Yii::$app->request->post();
        if (!isset($params['address_id'])) {
            $this->setHeader(400);
            return ['data' => [
                'status' => 'fail',
                'message' => 'parameter missing',
            ]];
        }
        $address = Address::findOne($params['address_id']);
        if ($user === null || $user->status != User::STATUS_PUBLISH || $address === null || $user->id != $address->user_id) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $carts = Cart::find()
            ->andWhere([
                'user_id' => $user->id,
                'status' => Cart::STATUS_PENDING
            ])
            ->all();
        $data = [];
        $foods = [];
        $errors = [];
        $prices = [];
        $restaurants = [];
        $totalFoodPrice = 0;
        $totalDeliveryPrice = 0;
        $totalDiscountedFoodPrice = 0;
        $totalDiscountedDeliveryPrice = 0;
        foreach ($carts as $cart) {
            !isset($prices[$cart->food->restaurant_id]['foods']) ? $prices[$cart->food->restaurant_id]['foods'] = 0 : null;
            $foods[$cart->food->restaurant_id][] = [
                'count' => $cart->count,
                'food' => [
                    'id' => $cart->food->id,
                    'create_time' => $cart->food->create_time,
                    'is_ready' => $cart->food->boolStatusName,
                    'name' => $cart->food->name,
                    'description' => $cart->food->description,
                    'display_price' => $cart->food->display_price,
                    'price' => $cart->food->price,
                    'stars' => $cart->food->cnt_comment > 0 ? (float)number_format($cart->food->sum_stars/$cart->food->cnt_comment, 2):null,
                    'raw_material' => $cart->food->raw_material,
                    'image' => $cart->food->imageUri
                ]
            ];
            if ($cart->food->status == Food::STATUS_ENDED) {
                $errors[] = [
                    'type' => 'food',
                    'id' => $cart->food_id,
                    'message' => 'موجودی غذای "'.$cart->food->name.'" به پایان رسیده است',
                ];
            } else {
                $prices[$cart->food->restaurant_id]['foods'] += $cart->food->display_price * $cart->count;
            }
        }
        foreach ($foods as $restaurantId => $cartFoods) {
            $restaurant = Restaurant::findOne($restaurantId);
            $restaurants[$restaurantId] = ['id' => $restaurantId, 'name' => $restaurant->name];
            if ($restaurant->status != Restaurant::STATUS_OPEN) {
                $errors[] = [
                    'type' => 'restaurant',
                    'id' => $restaurant->id,
                    'message' => 'ساعت کاری رستوران "'.$restaurant->name.'" به پایان رسیده است',
                ];
            }
            $distance = $this->distance($address, $restaurant);
            $deliveryCost = 1000 + (round($distance / 1000)) * 500;
            $prices[$restaurantId]['delivery'] = $deliveryCost;
            if (isset($params['delivery_type'][$restaurantId])) {
                switch ($params['delivery_type'][$restaurantId]) {
                    case 'restaurant':
                        if ($restaurant->has_delivery) {
                            $prices[$restaurantId]['delivery'] = $restaurant->delivery_cost;
                        } else {
                            $errors[] = [
                                'type' => 'delivery',
                                'id' => $restaurant->id,
                                'message' => 'رستوران "'.$restaurant->name.'" پیک برای ارسال غذا ندارد',
                            ];
                            $prices[$restaurantId]['delivery'] = 0;
                        }
                        break;
                    case 'presence':
                        $prices[$restaurantId]['delivery'] = 0;
                        break;
                    default:
                        $deliveryCost = 1000 + (round($distance / 1000)) * 500;
                        $prices[$restaurantId]['delivery'] = $deliveryCost;
                        break;
                }
            }
            $data[$restaurantId]['restaurant'] = $restaurants[$restaurantId];
            $data[$restaurantId]['foods'] = $foods[$restaurantId];
            $data[$restaurantId]['price'] = $prices[$restaurantId];
            $data[$restaurantId]['discounted_price'] = $prices[$restaurantId];
            $totalFoodPrice += $prices[$restaurantId]['foods'];
            $totalDeliveryPrice += $prices[$restaurantId]['delivery'];
        }
        $total['price']['foods'] = $totalFoodPrice;
        $total['price']['delivery'] = $totalDeliveryPrice;
        $total['price']['total'] = $totalFoodPrice + $totalDeliveryPrice;
        $total['discounted_price']['foods'] = $totalFoodPrice;
        $total['discounted_price']['delivery'] = $totalDeliveryPrice;
        $total['discounted_price']['total'] = $totalFoodPrice + $totalDeliveryPrice;
//        $discount = null;
        $goDiscount = true;
        $discount = new Discount();
        if (isset($params['discount_code']) && !empty(trim($params['discount_code']))) {
            $discount = Discount::find()->where(['coupon' => trim($params['discount_code']), 'status' => Discount::STATUS_PUBLISH])->one();
            if ($discount === null) {
                $errors[] = [
                    'type' => 'discount',
                    'message' => 'کد تخفیف منقضی شده است',
                ];
            } elseif (!empty($discount->start_time) && (time() - $discount->start_time) < 0) {
                $errors[] = [
                    'type' => 'discount',
                    'message' => 'زمان استفاده از این کد تخفیف هنوز فرانرسیده است',
                ];
                $goDiscount = false;
            } elseif (!empty($discount->end_time) && (time() - $discount->end_time) > 0) {
                $errors[] = [
                    'type' => 'discount',
                    'message' => 'کد تخفیف منقضی شده است',
                ];
                $goDiscount = false;
            }
        } elseif (!empty($user->inviter_id)) {
            $buys = Payment::find()
                ->where(['and',
                    ['user_id' => $user->id],
                    ['in', 'type', (new Payment)->getBuyPaymentTypes()]
                ])->count();
            if ($buys === 0) {
                $discount = Discount::find()->where(['coupon' => null, 'user_id' => $user->id, 'status' => Discount::STATUS_PENDING])->one();
            }
        }
        if ($discount && $goDiscount) {
            if (!empty($discount->over_than)) {
                switch ($discount->type) {
                    case $discount::TYPE_PERCENT_FOOD:
                    case $discount::TYPE_COST_FOOD:
                        if ($totalFoodPrice < $discount->over_than) {
                            $errors[] = [
                                'type' => 'discount',
                                'message' => 'این کد تخفیف برای سفارش‌های بالای ' . $discount->over_than . ' تومان است',
                            ];
                            $goDiscount = false;
                        }
                        break;
                    case $discount::TYPE_PERCENT_DELIVERY:
                    case $discount::TYPE_COST_DELIVERY:
                        if ($totalDeliveryPrice < $discount->over_than) {
                            $errors[] = [
                                'type' => 'discount',
                                'message' => 'این کد تخفیف برای هزینه‌ی پیک بالای ' . $discount->over_than . ' تومان است',
                            ];
                            $goDiscount = false;
                        }
                        break;
                    case $discount::TYPE_PERCENT_TOTAL:
                    case $discount::TYPE_COST_TOTAL:
                        if (($totalFoodPrice + $totalDeliveryPrice) < $discount->over_than) {
                            $errors[] = [
                                'type' => 'discount',
                                'message' => 'این کد تخفیف برای سفارش‌های بالای ' . $discount->over_than . ' تومان است',
                            ];
                            $goDiscount = false;
                        }
                        break;
                }
            }
            foreach ($data as $restaurantId => $row) {
                $discountPrices = $this->getDiscountedPrice(
                    $discount,
                    $totalFoodPrice,
                    $totalDeliveryPrice,
                    $data[$restaurantId]['price']['foods'],
                    $data[$restaurantId]['price']['delivery']
                );
                $data[$restaurantId]['discounted_price'] = $discountPrices;
                $totalDiscountedFoodPrice += $discountPrices['foods'];
                $totalDiscountedDeliveryPrice += $discountPrices['delivery'];
            }
            $total['discounted_price']['total'] = $totalDiscountedFoodPrice + $totalDiscountedDeliveryPrice;
        }

        $payment = Payment::find()->where(['and',
            [
                'status' => Factor::STATUS_PENDING,
                'user_id' => $user->id
            ], ['in', 'type', (new Payment)->getBuyPaymentTypes()]
        ])->one();
        if ($payment === null) {
            $payment = new Payment();
            $payment->user_id = $user->id;
            $payment->type = $payment::TYPE_ONLINE_FACTOR;
        }
        $payment->res_num = intval(microtime(true)*100);
        $payment->total_price = $total['price']['total'];
        $payment->total_discounted_price = $total['discounted_price']['total'];
        $payment->save();

        if (count($errors) == 0) {
            Cart::updateAll(['factor_id' => null], ['status' => Cart::STATUS_PENDING, 'user_id' => $user->id]);
            Factor::deleteAll(['payment_id' => $payment->id]);
            foreach ($data as $restaurantId => $row) {
                $factor = new Factor();
                $factor->status = Factor::STATUS_PENDING;
                $factor->price = $row['price']['foods'] + $row['price']['delivery'];
                $factor->discounted_price = $row['discounted_price']['foods'] + $row['discounted_price']['delivery'];
                if ($discount && $goDiscount) {
                    $factor->discount_id = $discount->id;
                }
                $factor->customer_id = $user->id;
                $factor->address_id = $address->id;
                $factor->restaurant_id = $restaurantId;
                $factor->payment_id = $payment->id;
                $factor->save();
                foreach ($row['foods'] as $eachFood) {
                    Cart::updateAll(['factor_id' => $factor->id], ['status' => Cart::STATUS_PENDING, 'user_id' => $user->id, 'food_id' => $eachFood['food']['id']]);
                }
            }
        }
        return ['data' => $data, 'total' => $total, 'errors' => $errors, 'payment_id' => (count($errors) == 0) ? $payment->res_num : null];
    }

    public function distance($address, $restaurant)
    {
        $distance = 10000;
        if (!empty($address->latitude) && !empty($address->longitude) && !empty($restaurant->latitude) && !empty($restaurant->longitude)) {
            $url = 'http://maps.googleapis.com/maps/api/directions/json?origin='.$restaurant->latitude.','.$restaurant->longitude.'&destination='.$address->latitude.','.$address->longitude.'&sensor=false';
            $gmap = file_get_contents($url);
            $gmap = json_decode($gmap);
            if (isset($gmap->routes[0]->legs[0])) {
                $distance = $gmap->routes[0]->legs[0]->distance->value;
            }
        }
        return $distance;
    }

    /**
     * @param $discount \common\models\Discount
     * @param int $totalFoodPrice
     * @param int $totalDeliveryPrice
     * @param int $foodPrice
     * @param int $deliveryCost
     * @return array
     */
    public function getDiscountedPrice($discount, $totalFoodPrice, $totalDeliveryPrice, $foodPrice, $deliveryCost)
    {
//        var_dump($foodPrice);
//        var_dump($deliveryCost);
//        exit();
        $amountFoods = 0;
        $amountDelivery = 0;
        switch ($discount->type) {
            case $discount::TYPE_PERCENT_FOOD:
                $amountFoods = ($discount->percent * $foodPrice) / 100;
                break;
            case $discount::TYPE_COST_FOOD:
                $amountFoods = $discount->price * $totalFoodPrice / $foodPrice;
                break;
            case $discount::TYPE_PERCENT_DELIVERY:
                $amountDelivery = ($discount->percent * $deliveryCost) / 100;
                break;
            case $discount::TYPE_COST_DELIVERY:
                $amountDelivery = $discount->price * $totalDeliveryPrice / $deliveryCost;
                break;
            case $discount::TYPE_PERCENT_TOTAL:
                $amountFoods = ($discount->percent * $foodPrice) / 100;
                $amountDelivery = ($discount->percent * $deliveryCost) / 100;
                break;
            case $discount::TYPE_COST_TOTAL:
                $amountFoods = $discount->price * ($totalDeliveryPrice + $totalFoodPrice) / $foodPrice;
                $amountDelivery = $discount->price * ($totalDeliveryPrice + $totalFoodPrice) / $deliveryCost;
                break;
        }
        return ['foods' => $foodPrice - $amountFoods, 'delivery' => $deliveryCost - $amountDelivery];
    }

    public function actionCheckDiscount($code)
    {

    }

	private function _getStatusCodeMessage($status)
    {
		$codes = [
			200 => 'OK',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
		];
		return (isset($codes[$status])) ? $codes[$status] : '';
	}
	
	private function setHeader($status)
    {
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		$content_type="application/json; charset=utf-8";

		header($status_header);
		header('Content-type: ' . $content_type);
		header('X-Powered-By: ' . "Berkeh <berkehgroup.ir>");
	}

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],$actions['create'],$actions['delete'],$actions['update'],$actions['view']);
        return $actions;
    }
}

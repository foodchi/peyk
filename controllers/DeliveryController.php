<?php

namespace api\controllers;

use common\models\Address;
use common\models\Restaurant;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use common\models\Delivery;
use common\models\User;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class DeliveryController extends ActiveController
{
    public $modelClass = 'common\models\Delivery';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        unset($behaviors['contentNegotiator']['formats']['application/xml']);
        return $behaviors;
    }

    public function actionUpdatePosition() {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        } else {
            return ['data' => [
                'status' => 'fail',
                'message' => 'Token fail',
            ]];
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $params = Yii::$app->request->post();
        if (!isset($params['latitude']) || !isset($params['longitude'])) {
            $this->setHeader(400);
            return ['data' => ['status' => 'fail', 'message'=>'invalid parameters']];
        }

        $delivery = Delivery::find()->where(["user_id"=>$user->id])->one();
        if ($delivery === null) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'user is not deliverer']];
        }
        $delivery->latitude = $params['latitude'];
        $delivery->longitude = $params['longitude'];
        $delivery->update_time = time();

        if ($delivery->save()) {
            return ['data' => [
                'status' => 'success',
                'message' => 'position updated',
                'delivery' => [
                    'update_time' => $delivery->update_time,
                    'status' => $delivery->status,
                    'latitude' => $delivery->latitude,
                    'longitude' => $delivery->latitude,
                ]
            ]];
        }
        return ['data' => [
            'status' => 'fail',
            'message' => 'invalid parameters',
        ]];
    }

    public function actionStatus() {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $params = Yii::$app->request->post();
        if (!isset($params['status'])) {
            $this->setHeader(400);
            return ['data' => ['status' => 'fail', 'message'=>'invalid parameters']];
        }
        $delivery = Delivery::find()->where(["user_id"=>$user->id])->one();
        if ($delivery === null) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'user is not deliverer']];
        }
        if ($delivery->status != Delivery::STATUS_PENDING && in_array($params['status'], [Delivery::STATUS_PUBLISH, Delivery::STATUS_DISABLE])) {
            $delivery->status = $params['status'];
            if ($delivery->save()) {
                return ['data' => [
                    'status' => 'success',
                    'message' => 'position updated',
                    'delivery' => [
                        'update_time' => $delivery->update_time,
                        'status' => $delivery->status,
                        'latitude' => $delivery->latitude,
                        'longitude' => $delivery->latitude,
                    ]
                ]];
            }
        }
        $this->setHeader(400);
        return ['data' => [
            'status' => 'fail',
            'message' => 'invalid parameters',
        ]];
    }

    public function actionCost($address_id, array $restaurant_ids)
    {
        if (is_array($restaurant_ids)) {
            foreach ($restaurant_ids as $restaurant_id) {
                $restaurant = Restaurant::findOne($restaurant_id);
                if ($restaurant == null) {
                    $this->setHeader(404);
                    return ['data' => [
                        'status' => 'fail',
                        'message' => 'restaurant not found',
                    ]];
                }
                $address = Address::findOne($address_id);
                if ($address == null) {
                    $this->setHeader(404);
                    return ['data' => [
                        'status' => 'fail',
                        'message' => 'address not found',
                    ]];
                }

                $duration = 0;
                if (!empty($address->latitude) && !empty($address->longitude) && !empty($restaurant->latitude) && !empty($restaurant->longitude)) {
                    $url = 'http://maps.googleapis.com/maps/api/directions/json?origin='.$restaurant->latitude.','.$restaurant->longitude.'&destination='.$address->latitude.','.$address->longitude.'&sensor=false';
                    $gmap = file_get_contents($url);
                    $gmap = json_decode($gmap);
                    if (isset($gmap->routes[0]->legs[0])) {
                        $distance = $gmap->routes[0]->legs[0]->distance->value;
                        $duration = $gmap->routes[0]->legs[0]->duration->value;
                    } else {
                        $duration = 0;
                    }
                }
                if (isset($distance)) {
                    $foodchiCost = 1000 + (round($distance/1000))*500;
                } else {
                    $foodchiCost = 5000;
                }

                $options = [];
                $options[] = [
                    'deliver_by' => 'foodchi',
                    'description' => 'تحویل غذا توسط پیک فودچی',
                    'cost' => $foodchiCost,
                    'duration' => (int)($duration * 1.3),
                ];
                if ($restaurant->has_delivery) {
                    $options[] = [
                        'deliver_by' => 'restaurant',
                        'description' => 'تحویل غذا توسط پیک رستوران',
                        'cost' => $restaurant->delivery_cost,
                        'duration' => (int)($duration * 1.5),
                    ];
                }
                $options[] = [
                    'deliver_by' => 'presence',
                    'description' => 'تحویل غذا به صورت حضوری',
                    'cost' => 0,
                    'duration' => 0,
                ];

                $out[] = [
                    'restaurant_id' => $restaurant_id,
                    'options' => $options
                ];
            }
            return ['data' => $out];
        }
    }

    private function _getStatusCodeMessage($status) {
		$codes = [
			200 => 'OK',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
		];
		return (isset($codes[$status])) ? $codes[$status] : '';
	}

	private function setHeader($status) {
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		$content_type="application/json; charset=utf-8";

		header($status_header);
		header('Content-type: ' . $content_type);
		header('X-Powered-By: ' . "Berkeh <berkehgroup.ir>");
	}

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],$actions['create'],$actions['delete'],$actions['update'],$actions['view']);
        return $actions;
    }
}

<?php

namespace api\controllers;

use yii\rest\ActiveController;
use yii\web\Response;
use common\models\User;
use common\models\Comment;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CommentController extends ActiveController
{
    public $modelClass = 'common\models\Comment';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        unset($behaviors['contentNegotiator']['formats']['application/xml']);
        return $behaviors;
    }
    
    public function actionIndex($restaurant_id = null, $food_id = null) {
        if ((bool)$restaurant_id === (bool)$food_id) {
            $this->setHeader(400);
            echo json_encode(array('error'=>['code'=>400,'message'=>'parameter missing or invalid']),JSON_PRETTY_PRINT);
            exit();
        }

        $page = 1;
        $limit = 20;

        if (isset(\Yii::$app->request->queryParams['page'])) {
            $page = (int)\Yii::$app->request->queryParams['page'];
        }
        $offset = $limit*($page-1);

        if (!empty($restaurant_id)) {
            $condition = [
                'restaurant_id' => $restaurant_id,
                'status' => Comment::STATUS_PUBLISH
            ];
        } else {
            $condition = [
                'food_id' => $food_id,
                'status' => Comment::STATUS_PUBLISH
            ];
        }
        $comments = Comment::find()
            ->where($condition);
        $count = (int)$comments->count();
        $comments = $comments
            ->orderBy("create_time DESC")
            ->offset($offset)
            ->limit($limit)
            ->all();

        $data = [];
        foreach ($comments as $comment) {
            $eachComment = [
                'id' => $comment->id,
                'create_time' => $comment->create_time,
                'star' => $comment->star,
                'name' => $comment->name,
                'text' => $comment->text,
                'user_id' => $comment->user_id,
            ];
            if (!empty($restaurant_id)) {
                $eachComment['restaurant_id'] = $comment->restaurant_id;
            } else {
                $eachComment['food_id'] = $comment->food_id;
            }
            $data[] = $eachComment;
        }

        $meta = [
            'totalCount' => $count,
            'pageCount' => ceil($count/20),
            'currentPage' => $page,
            'perPage' => 20
        ];
        return ['data' => $data, '_meta' => $meta];        
    }

    public function actionCreate() {
        $params = $_POST;
        $user = null;
        $h = getallheaders();
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            echo json_encode(array('error'=>['code'=>401,'message'=>'Unauthorized']),JSON_PRETTY_PRINT);
            exit();
        }
        if (isset($params['star']) && !empty($params['star']) && is_numeric($params['star'])
            && isset($params['text']) && !empty($params['text'])
            && (
                (isset($params['restaurant_id']) && !empty($params['restaurant_id']))
                || (isset($params['food_id']) && !empty($params['food_id']))
                )
            ) {
            $comment = new Comment();
            $comment->text = $params['text'];
            $comment->star = $params['star'];
            if (isset($params['restaurant_id']) && !empty($params['restaurant_id'])) {
                $comment->restaurant_id = $params['restaurant_id'];
            } else {
                $comment->food_id = $params['food_id'];
            }
            $comment->user_id = $user->id;
            $comment->name = (isset($params['name']) && !empty($params['name'])) ? $params['name'] : $user->name.' '.$user->family;
            if ($comment->save()) {
                $this->setHeader(201);
                return ['data' => [
                    'status' => 'success',
                    'message' => 'comment saved and pending to publish',
                ]];
            } else {
                $this->setHeader(400);
                echo json_encode(array('error'=>['code'=>400,'message'=>'Invalid Parameters']),JSON_PRETTY_PRINT);
                exit();
            }
        } else {
            $this->setHeader(400);
            echo json_encode(array('error'=>['code'=>400,'message'=>'parameter missing']),JSON_PRETTY_PRINT);
            exit();
        }
    }

    private function _getStatusCodeMessage($status) {
        $codes = [
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        ];
        return (isset($codes[$status])) ? $codes[$status] : '';
    }
    
    private function setHeader($status) {
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        $content_type="application/json; charset=utf-8";

        header($status_header);
        header('Content-type: ' . $content_type);
        header('X-Powered-By: ' . "Berkeh <berkehgroup.ir>");
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],$actions['update'],$actions['delete'],$actions['view'],$actions['create']);
        // $actions['view']['defaultFields'] = function () {
        //     return ['id','title'];
        // };
        return $actions;
    }
}

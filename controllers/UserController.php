<?php

namespace api\controllers;

use common\models\Delivery;
use common\models\Discount;
use common\models\Payment;
use common\models\Restaurant;
use common\models\Setting;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use api\models\SignupForm;
use yii\filters\VerbFilter;
use common\models\User;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class UserController extends ActiveController
{
    public $modelClass = 'common\models\User';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'signup' => ['post'],
                'login' => ['post'],
                'change-password' => ['post'],
                'check-token' => ['post'],
                'reset-password-request' => ['post'],
                'reset-password' => ['post'],
            ],
        ];

        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        unset($behaviors['contentNegotiator']['formats']['application/xml']);
        return $behaviors;
    }

    public function actionSignup() {
        $params = Yii::$app->request->post();
        if (!isset($params['username']) || !isset($params['password'])) {
            return ['data' => [
                'status' => 'fail',
                'message' => 'username or password is empty'
            ]];
        }
        $model = new SignupForm();
        $model->username = $params['username'];
        $model->password = $params['password'];
        $model->regId = (isset($params['reg_id']) && !empty($params['reg_id'])) ? $params['reg_id']:null;
        if (isset($params['invited_by'])) {
            $inviter = User::find()->where(['invite_name' => $params['invited_by']])->one();
            if ($inviter) {
                $model->inviter_id = $inviter->id;
            }
        }

        if ($user = $model->signup()) {
            $fields = [
                'user' => '09356665530',
                'pass' => 'typesara9182',
                'to' => $user->username,
                'text' => 'به فودچی خوش آمدید. کد تایید: '.substr($user->token, 20, 4),
                'lineNo' => '3000485530'
            ];
            $ch = curl_init('http://ip.sms.ir/SendMessage.ashx');
            curl_setopt_array($ch,[
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $fields,
                CURLOPT_RETURNTRANSFER => true
            ]);
            $resposne = curl_exec($ch);
            curl_close($ch);

            return ['data' => [
                'status' => 'success',
                'message' => 'validation code has been sent',
                'token' => $user->token
                ]];
        }
        $message = '';
        if ($model->errors) {
            foreach ($model->errors as $errors) {
                foreach ($errors as $error) {
                    $message .= $error."\n";
                }
            }
            $message = str_replace(['Username', 'Password'], ['شماره موبایل', 'رمز عبور'], $message);
        }
        return ['data' => ['status' => 'fail', 'message' => $message]];
    }

    public function actionInviteName()
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        return ['invite_name' => $user->invite_name];
    }

    public function actionConfirm($code) {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user) {
            if ($user->status == User::STATUS_PENDING && substr($user->token, 20, 4) == $code) {
                $user->status = User::STATUS_PUBLISH;
                $user->save();
                return ['data' => [
                    'status' => 'success',
                    'message' => 'user cofirmed'
                ]];
            }
            return ['data' => [
                'status' => 'fail',
                'message' => 'confirm code is invalid or user is confirmed before'
            ]];
        }
        return ['data' => [
            'status' => 'fail',
            'message' => 'token is invalid'
        ]];
    }

    public function actionUpdate() {
        $params = Yii::$app->request->post();
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        if (isset($params['invited_by']) && $user->inviter_id !== null) {
            $this->setHeader(400);
            return ['data' => [
                'status' => 'fail',
                'message' => 'invited by user cannot be changed',
            ]];
        } elseif (isset($params['invited_by'])) {
            $inviter = User::find()->where(['invite_name' => $params['invited_by']])->one();
            if ($inviter) {
                $user->inviter_id = $inviter->id;
                $setting = Setting::find()->where(['item' => Setting::INVITED_REWARD])->one();
                if ($setting) {
                    $discount = new Discount();
                    $discount->type = $setting->type;
                    $discount->percent = $setting->percent;
                    $discount->price = $setting->price;
                    $discount->user_id = $user->id;
                    $discount->save();
                }
            } else {
                $this->setHeader(400);
                return ['data' => [
                    'status' => 'fail',
                    'message' => 'invited by name is invalid',
                ]];
            }
        }
        $user->email = isset($params['email']) ? $params['email'] : $user->email;
        $user->name = isset($params['name']) ? $params['name'] : $user->name;
        $user->family = isset($params['family']) ? $params['family'] : $user->family;
        $user->reg_id = isset($params['reg_id']) ? $params['reg_id'] : $user->reg_id;
        $user->info = isset($params['info']) ? $params['info'] : $user->info;
        $user->invite_name = isset($params['invite_name']) ? $params['invite_name'] : $user->invite_name;
        if ($user->save()) {
            return ['data' => [
                'status' => 'success',
                'message' => 'user data updated',
                'user' => [
                    'username' => $user->username,
                    'invite_name' => $user->invite_name,
                    'email' => $user->email,
                    'name' => $user->name,
                    'family' => $user->family,
                    'token' => $user->token,
                    'balance' => $user->balance,
                ]
            ]];
        } else {
            $this->setHeader(400);
            return ['data' => [
                'status' => 'fail',
                'message' => 'some parameter(s) is invalid',
            ]];
        }
    }

    public function actionNotification() {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        if (!Yii::$app->request->isGet) {
            $params = Yii::$app->request->post();
            $user->confirm_notif = isset($params['confirm_notification']) ? $params['confirm_notification'] : $user->confirm_notif;
            $user->shipping_notif = isset($params['shipping_notification']) ? $params['shipping_notification'] : $user->shipping_notif;
            $user->delivered_notif = isset($params['delivered_notification']) ? $params['delivered_notification'] : $user->delivered_notif;
            $user->survey_notif = isset($params['survey_notification']) ? $params['survey_notification'] : $user->survey_notif;
            $user->contact_notif = isset($params['contact_notification']) ? $params['contact_notification'] : $user->contact_notif;
            if (!$user->save()) {
                $this->setHeader(400);
                return ['data' => [
                    'status' => 'fail',
                    'message' => 'some parameters are invalid'
                ]];
            }
        }
        return ['data' => [
            'status' => 'success',
//            'message' => 'notification setting updated',
            'user' => [
                'confirm_notification' => intval($user->confirm_notif),
                'shipping_notification' => intval($user->shipping_notif),
                'delivered_notification' => intval($user->delivered_notif),
                'survey_notification' => intval($user->survey_notif),
                'contact_notification' => intval($user->contact_notif),
            ]
        ]];
    }

    public function actionLogin()
    {
        $params = Yii::$app->request->post();
        if (!isset($params['username']) || !isset($params['password'])) {
            return ['data' => [
                'status' => 'fail',
                'message' => 'username or password is empty'
            ]];
        }
        $user = User::find()->where(['username' => $params['username']])->limit(1)->one();
        if ($user && $user->validatePassword($params['password'])) {
            if (isset($params['role'])) {
                $count = 0;
                if ($params['role'] == 'delivery') {
                    $count = Delivery::find()->where(['user_id' => $user->id])->count();
                } elseif ($params['role'] == 'restaurant') {
                    $count = Restaurant::find()->where(['user_id' => $user->id])->count();
                } elseif ($params['role'] == 'user') {
                    $count = 1;
                }
                if ($count == 0) {
                    return ['data' => [
                        'status' => 'fail',
                        'message' => 'user have not associated role',
                    ]];
                }
            }
            return ['data' => [
                'status' => 'success',
                'message' => 'login data is correct',
                'token' => $user->token,
                'user' => [
                    'username' => $user->username,
                    'invite_name' => $user->invite_name,
                    'email' => $user->email,
                    'name' => $user->name,
                    'family' => $user->family,
                    'token' => $user->token,
                    'balance' => $user->balance,
                ]
            ]];
        }
        return ['data' => [
            'status' => 'fail',
            'message' => 'username and/or password is invalid',
        ]];
    }

    public function actionView()
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }

        $discountAmount = Payment::find()
            ->select('SUM(total_price-total_discounted_price) as discountAmount')
            ->where(['user_id' => $user->id, 'status' => Payment::STATUS_PUBLISH])
            ->one();

        $orderCount = Payment::find()
            ->where(['user_id' => $user->id, 'status' => Payment::STATUS_PUBLISH])
            ->count();

        return [
            'username' => $user->username,
            'invite_name' => $user->invite_name,
            'email' => $user->email,
            'name' => $user->name,
            'family' => $user->family,
            'token' => $user->token,
            'reg_id' => $user->reg_id,
            'balance' => $user->balance,
            'discount' => intval($discountAmount->discountAmount),
            'finished_orders' => intval($orderCount),
        ];
    }

    public function actionChangePassword()
    {
        $h = getallheaders();
        $user = null;
        if (isset($h['X-Mobile-Token']) && !empty($h['X-Mobile-Token'])) {
            $user = User::find()->where(['token' => $h['X-Mobile-Token']])
                ->limit(1)
                ->one();
        }
        if ($user === null || $user->status != User::STATUS_PUBLISH) {
            $this->setHeader(401);
            return ['data' => ['status' => 'fail', 'message'=>'Unauthorized']];
        }
        $params = Yii::$app->request->post();
        if (isset($params['old_password']) && isset($params['new_password'])) {
            if ($user->validatePassword($params['old_password'])) {
                $user->setPassword($params['new_password']);
                $user->save();
                return ['data' => [
                    'status' => 'success',
                    'message' => 'password changed',
                ]];
            } else {
                return ['data' => [
                    'status' => 'fail',
                    'message' => 'old password is incorrect',
                ]];
            }
        } else {
            $this->setHeader(400);
            return ['data' => [
                'status' => 'fail',
                'message' => 'parameter(s) missing',
            ]];
        }
    }

    public function actionResetPasswordRequest()
    {
        $params = Yii::$app->request->post();
        if (isset($params['username'])) {
            $user = User::find()->where(['username' => $params['username']])->limit(1)->one();
            if ($user) {
                $rand = rand(10000,99999);
                $user->password_reset_token = $rand;
                $user->save();
                $fields = [
                    'user' => '09356665530',
                    'pass' => 'typesara9182',
                    'to' => $user->username,
                    'text' => 'کد فراموشی رمز عبور: '.$rand,
                    'lineNo' => '3000485530'
                ];
                $ch = curl_init('http://ip.sms.ir/SendMessage.ashx');
                curl_setopt_array($ch,[
                    CURLOPT_POST => 1,
                    CURLOPT_POSTFIELDS => $fields,
                    CURLOPT_RETURNTRANSFER => true
                ]);
                $resposne = curl_exec($ch);
                curl_close($ch);

                return ['data' => [
                    'status' => 'success',
                    'message' => 'message sent',
                ]];
            } else {
                return ['data' => [
                    'status' => 'fail',
                    'message' => 'username is incorrect',
                ]];
            }
        } else {
            $this->setHeader(400);
            return ['data' => [
                'status' => 'fail',
                'message' => 'parameter(s) missing',
            ]];
        }
    }

    public function actionCheckToken()
    {
        $params = Yii::$app->request->post();
        if (isset($params['reset_token'], $params['username'])) {
            $user = User::find()->where([
                'password_reset_token' => $params['reset_token'],
                'username' => $params['username']
            ])->limit(1)->one();
            if ($user) {
                return ['data' => [
                    'status' => 'success',
                    'message' => 'token is valid',
                ]];
            } else {
                return ['data' => [
                    'status' => 'fail',
                    'message' => 'invalid token',
                ]];
            }
        } else {
            $this->setHeader(400);
            return ['data' => [
                'status' => 'fail',
                'message' => 'parameter(s) missing',
            ]];
        }
    }

    public function actionResetPassword()
    {
        $params = Yii::$app->request->post();
        if (isset($params['reset_token'], $params['username'], $params['password'])) {
            $user = User::find()->where([
                'password_reset_token' => $params['reset_token'],
                'username' => $params['username']
            ])->limit(1)->one();
            if ($user) {
                $user->setPassword($params['password']);
                $user->password_reset_token = null;
                $user->save();
                return ['data' => [
                    'status' => 'success',
                    'message' => 'password changed',
                ]];
            } else {
                return ['data' => [
                    'status' => 'fail',
                    'message' => 'reset token is incorrect',
                ]];
            }
        } else {
            $this->setHeader(400);
            return ['data' => [
                'status' => 'fail',
                'message' => 'parameter(s) missing',
            ]];
        }
    }

    private function _getStatusCodeMessage($status) {
        $codes = [
            200 => 'OK',
            201 => 'Created',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        ];
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    private function setHeader($status) {
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        $content_type="application/json; charset=utf-8";

        header($status_header);
        header('Content-type: ' . $content_type);
        header('X-Powered-By: ' . "Berkeh <berkehgroup.ir>");
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],$actions['create'],$actions['delete'],$actions['update'],$actions['view']);
        return $actions;
    }

}

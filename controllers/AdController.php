<?php

namespace api\controllers;

use common\models\Ad;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class AdController extends ActiveController
{
    public $modelClass = 'common\models\Ad';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        unset($behaviors['contentNegotiator']['formats']['application/xml']);
        return $behaviors;
    }

    public function actionIndex() {
        $ads = Ad::find()->all();//->orderBy('priority')->groupBy('position')->all();
//        var_dump($ads);
        $adArray = [];
        foreach ($ads as $ad) {
            $adArray[] = [
                'id' => $ad->id,
                'position' => $ad->position,
                'priority' => $ad->priority,
                'url' => $ad->url,
                'restaurant_id' => $ad->restaurant_id,
                'food_id' => $ad->food_id,
                'image' => $ad->imageUri
            ];
        }
        return ['data' => $adArray];
    }


	private function _getStatusCodeMessage($status) {
		$codes = [
			200 => 'OK',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
		];
		return (isset($codes[$status])) ? $codes[$status] : '';
	}
	
	private function setHeader($status) {
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		$content_type="application/json; charset=utf-8";

		header($status_header);
		header('Content-type: ' . $content_type);
		header('X-Powered-By: ' . "Berkeh <berkehgroup.ir>");
	}

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],$actions['create'],$actions['delete'],$actions['update'],$actions['view']);
        return $actions;
    }
}

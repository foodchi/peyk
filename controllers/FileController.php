<?php

namespace api\controllers;

use Yii;
use common\models\File;
use common\models\FileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

use yii\imagine\Image;
// use Imagine\Gd;
// use Imagine\Image\Box;
// use Imagine\Image\BoxInterface;
/**
 * FileController implements the CRUD actions for File model.
 */
class FileController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionImage($id, $width=null, $height=null, $resize=File::RESIZE_INSIDE, $title=null)
    {
        $file = File::find()->where(['uri'=>$id])->select(['name', 'path'])->limit(1)->one();
        if ($file === null) {
            return null;
        }
        $mime = mime_content_type($file->basePath."/".$file->path);
        header("Content-Type: ".$mime);

        $image = Image::getImagine();
        $newImage = $image->open($file->basePath."/".$file->path);
        $newImage->show(substr($file->path, (strpos($file->path, ".") + 1)));
        exit();
    }
}

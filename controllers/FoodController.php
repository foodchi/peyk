<?php

namespace api\controllers;

use yii\rest\ActiveController;
use yii\web\Response;
use common\models\Food;
use common\models\FoodSearch;
use common\models\Restaurant;
use common\models\Category;
// use common\models\ContentCategory;
// use common\models\Mobile;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class FoodController extends ActiveController
{
    public $modelClass = 'common\models\Food';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        unset($behaviors['contentNegotiator']['formats']['application/xml']);
        return $behaviors;
    }

    public function actionRestaurant($restaurant_id) {
        $restaurant = Restaurant::findOne((int)$restaurant_id);
        if ($restaurant === null) {
            return ['data' => [
                'status' => 'fail',
                'message' => 'not found',
            ]];
        }
        $categories = Category::find()
            ->where(['type' => Category::TYPE_FOOD])
            ->orderBy('priority')
            ->all();
        $data = [];
        if ($categories) {
            foreach ($categories as $category) {
                $foods = Food::find()
                    ->where(['category_id' => $category->id, 'restaurant_id' => $restaurant->id])
                    ->all();
                if (count($foods) > 0) {
                    $foodArray = [];
                    foreach ($foods as $food) {
                        $foodArray[] = [
                            'id' => $food->id,
                            'create_time' => $food->create_time,
                            'is_ready' => $food->boolStatusName,
                            'category' => $food->category->name,
                            'name' => $food->name,
                            'description' => $food->description,
                            'display_price' => $food->display_price,
                            'price' => $food->price,
                            'stars' => $food->cnt_comment > 0 ? (float)number_format($food->sum_stars/$food->cnt_comment, 2):null,
                            'raw_material' => $food->raw_material,
                            'image' => $food->imageUri
                        ];

                    }
                    $data[] = [
                        'category' => $category->name,
                        'foods' => $foodArray,
                    ];
                }
            }
        }
        return ['data' => $data];
    }

    public function actionIndex($restaurant_id) {
        $page = 1;
        $limit = 20;
        if (isset(\Yii::$app->request->queryParams['page'])) {
            $page = (int)\Yii::$app->request->queryParams['page'];
        }
        $offset = $limit*($page-1);
        $foods = Food::find()
            ->andWhere(['in', 'food.status', [Food::STATUS_READY, Food::STATUS_ENDED]])
            ->andWhere(['restaurant_id' => $restaurant_id]);
        $count = (int)$foods->count();
        $foods = $foods
            ->offset($offset)
            ->limit($limit)
            // ->orderBy('`food`.`status`, `sum_stars`/`cnt_comment`')
            ->all();
        $data = [];
        foreach ($foods as $food) {
            $data[] = [
                'id' => $food->id,
                'is_ready' => $food->boolStatusName,
                'category' => $food->category->name,
                'name' => $food->name,
                'description' => $food->description,
                'display_price' => $food->display_price,
                'price' => $food->price,
                'stars' => $food->cnt_comment > 0 ? (float)number_format($food->sum_stars/$food->cnt_comment, 2):null,
                'raw_material' => $food->raw_material,
                'image' => $food->imageUri,
            ];
        }
        $meta = [
            'totalCount' => $count,
            'pageCount' => ceil($count/20),
            'currentPage' => $page,
            'perPage' => 20
        ];
        return ['data' => $data, '_meta' => $meta];
    }

    public function actionTop($by = 'star', $category_id = null)
    {
        $page = 1;
        $limit = 20;
        if (isset(\Yii::$app->request->queryParams['page'])) {
            $page = (int)\Yii::$app->request->queryParams['page'];
        }
        $offset = $limit*($page-1);
        
        $foods = Food::find()
            // ->select(['id', 'name', 'description', 'price', 'image_id', 'cover' 'logo_id', 'category_id'])
            ->joinWith('restaurant')
            ->andWhere(['in', 'food.status', [Food::STATUS_READY, Food::STATUS_ENDED]])
            ->andWhere(['in', 'restaurant.status', [Restaurant::STATUS_OPEN, Restaurant::STATUS_CLOSED]]);
        if ($category_id) {
            $foods = $foods->andWhere(['restaurant.category_id' => (int)$category_id]);
        }
        $count = (int)$foods->count();
        $foods = $foods
            ->offset($offset)
            ->limit($limit)
            ->orderBy('restaurant.status, `food`.`sum_stars`/`food`.`cnt_comment`')
            ->all();
        $data = [];
        foreach ($foods as $food) {
            $data[] = [
                'id' => $food->id,
                'is_ready' => $food->boolStatusName,
                'category' => $food->category->name,
                'name' => $food->name,
                'description' => $food->description,
                'display_price' => $food->display_price,
                'price' => $food->price,
                'stars' => $food->cnt_comment > 0 ? (float)number_format($food->sum_stars/$food->cnt_comment, 2):null,
                'raw_material' => $food->raw_material,
                'image' => $food->imageUri,
                'restaurant' => [
                    'id' => $food->restaurant->id,
                    'name' => $food->restaurant->name,
                    'stars' => $food->restaurant->cnt_comment > 0 ? (float)number_format($food->restaurant->sum_stars/$food->restaurant->cnt_comment, 2):null,
                    'is_open' => $food->restaurant->boolStatusName,
                    'has_delivery' => (bool)$food->restaurant->has_delivery,
                    'delivery_cost' => $food->restaurant->delivery_cost,
                    'preparation_time' => $food->restaurant->preparation_time,
                    'capacity' => 10,
                    'self_discount' => $food->restaurant->self_discount,
                    'logo' => $food->restaurant->getimageUriById($food->restaurant->logo_id),
                    // 'cover' => $food->restaurant->getimageUriById($food->restaurant->cover_id),
                    'category_id' => $food->restaurant->category_id,
                ]
            ];
        }
        $meta = [
            'totalCount' => $count,
            'pageCount' => ceil($count/20),
            'currentPage' => $page,
            'perPage' => 20
        ];
        return ['data' => $data, '_meta' => $meta];
    }

    public function actionSearch($q) {
        $page = 1;
        $limit = 20;
        if (isset(\Yii::$app->request->queryParams['page'])) {
            $page = (int)\Yii::$app->request->queryParams['page'];
        }
        $offset = $limit*($page-1);
        $foods = Food::find()
            // ->select(['id', 'name', 'description', 'price', 'image_id', 'cover_id'])
            ->joinWith('restaurant')
            ->andWhere(['in', 'food.status', [Food::STATUS_READY, Food::STATUS_ENDED]])
            ->andWhere(['in', 'restaurant.status', [Restaurant::STATUS_OPEN, Restaurant::STATUS_CLOSED]])
            ->andWhere([
                'or',
                ['like', 'food.name', $q],
                ['like', 'food.description', $q],
            ]);
        $count = (int)$foods->count();
        $foods = $foods
            ->offset($offset)
            ->orderBy('`food`.`sum_stars`/`food`.`cnt_comment`')
            ->limit($limit)
            ->all();
        $data = [];
        foreach ($foods as $food) {
            $data[] = [
                'id' => $food->id,
                'is_ready' => $food->boolStatusName,
                'category' => $food->category->name,
                'name' => $food->name,
                'description' => $food->description,
                'display_price' => $food->display_price,
                'price' => $food->price,
                'stars' => $food->cnt_comment > 0 ? (float)number_format($food->sum_stars/$food->cnt_comment, 2):null,
                'raw_material' => $food->raw_material,
                'image' => $food->imageUri,
                'cover' => $food->getImageUriById($food->cover_id),
                'restaurant' => [
                    'id' => $food->restaurant->id,
                    'name' => $food->restaurant->name,
                    'stars' => $food->restaurant->cnt_comment > 0 ? (float)number_format($food->restaurant->sum_stars/$food->restaurant->cnt_comment, 2):null,
                    'is_open' => $food->restaurant->boolStatusName,
                    'has_delivery' => (bool)$food->restaurant->has_delivery,
                    'delivery_cost' => $food->restaurant->delivery_cost,
                    'preparation_time' => $food->restaurant->preparation_time,
                    'capacity' => 10,
                    'self_discount' => $food->restaurant->self_discount,
                    'logo' => $food->restaurant->getimageUriById($food->restaurant->logo_id),
                    'category_id' => $food->restaurant->category_id,
                ]
            ];
        }
        $meta = [
            'totalCount' => $count,
            'pageCount' => ceil($count/20),
            'currentPage' => $page,
            'perPage' => 20
        ];
        return ['data' => $data, '_meta' => $meta];
    }

    public function actionView($id)
    {
        $model = Food::findOne($id);
        if ($model !== null) {
            return [
                'id' => $model->id,
                'create_time' => $model->create_time,
                'is_ready' => $model->boolStatusName,
                'category' => $model->category->name,
                'name' => $model->name,
                'description' => $model->description,
                'display_price' => $model->display_price,
                'price' => $model->price,
                'stars' => $model->cnt_comment > 0 ? (float)number_format($model->sum_stars/$model->cnt_comment, 2):null,
                'raw_material' => $model->raw_material,
                'image' => $model->imageUri,
                'cover' => $model->getimageUriById($model->cover_id),
                'restaurant' => [
                    'id' => $model->restaurant->id,
                    'name' => $model->restaurant->name,
                    'stars' => $model->restaurant->cnt_comment > 0 ? (float)number_format($model->restaurant->sum_stars/$model->restaurant->cnt_comment, 2):null,
                    'is_open' => $model->restaurant->boolStatusName,
                    'has_delivery' => (bool)$model->restaurant->has_delivery,
                    'delivery_cost' => $model->restaurant->delivery_cost,
                    'preparation_time' => $model->restaurant->preparation_time,
                    'capacity' => 10,
                    'self_discount' => $model->restaurant->self_discount,
                    'logo' => $model->restaurant->getimageUriById($model->restaurant->logo_id),
                    'category_id' => $model->restaurant->category_id,
                ]
            ];
        } else {
            return ['data' => [
                'status' => 'fail',
                'message' => 'not found',
            ]];
        }
    }

	private function _getStatusCodeMessage($status) {
		$codes = [
			200 => 'OK',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
		];
		return (isset($codes[$status])) ? $codes[$status] : '';
	}
	
	private function setHeader($status) {
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		$content_type="application/json; charset=utf-8";

		header($status_header);
		header('Content-type: ' . $content_type);
		header('X-Powered-By: ' . "Berkeh <berkehgroup.ir>");
	}

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'],$actions['create'],$actions['delete'],$actions['update'],$actions['view']);
        return $actions;
    }
}

<?php

namespace api\controllers;

use yii\rest\ActiveController;
use yii\web\Response;
use common\models\Category;
use common\models\CategorySearch;
// use common\models\Content;
// use common\models\ContentCategory;
// use common\models\Mobile;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends ActiveController
{
    public $modelClass = 'common\models\Category';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        unset($behaviors['contentNegotiator']['formats']['application/xml']);
        return $behaviors;
    }

    public function actionIndex() {
        $data = [];
        $categories = Category::find()
            ->select(['id', 'name'])
            ->where(['status' => Category::STATUS_PUBLISH, 'type' => Category::TYPE_RESTAURANT])
            ->orderBy('priority')->all();
        return ['data' => $categories];
    }

	private function _getStatusCodeMessage($status) {
		$codes = [
			200 => 'OK',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
		];
		return (isset($codes[$status])) ? $codes[$status] : '';
	}
	
	private function setHeader($status) {
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		$content_type="application/json; charset=utf-8";

		header($status_header);
		header('Content-type: ' . $content_type);
		header('X-Powered-By: ' . "Berkeh <berkehgroup.ir>");
	}

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create'],$actions['delete'],$actions['update'],$actions['view']);
        $actions['index']['prepareDataProvider'] = [$this, 'categories'];
        return $actions;
    }

    public function categories() 
    {
        $searchModel = new CategorySearch();
        array_merge(\Yii::$app->request->queryParams, ['']);
        $provider = $searchModel->search(\Yii::$app->request->queryParams);
        $provider->query->select(['id', 'name'])
            ->where(['status' => Category::STATUS_PUBLISH, 'type' => Category::TYPE_RESTAURANT])
            ->orderBy('priority')->all();
        return $provider;
    }
}

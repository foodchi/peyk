<?php

namespace api\models;
use common\models\Main;
use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $password;
    public $inviter_id;
    public $regId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'نام کاربری تکراری است.'],
            ['username', 'integer', 'min' => 9010000000, 'max' => 9999999999],
            ['inviter_id', 'integer'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['regId', 'string', 'min' => 100, 'max' => 256],
        ];
    }
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->inviter_id = $this->inviter_id;
            $user->invite_name = rand(10000000,99999999);
            $user->reg_id = $this->regId;
 			$user->status = User::STATUS_PENDING;
            $user->setPassword($this->password);
            $user->token = md5(uniqid());
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }
        return null;
    }
}
